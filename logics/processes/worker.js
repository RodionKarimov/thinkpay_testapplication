const cluster = require ( 'cluster' );
const redis = require ( "redis" );
const moduleAsyncRedis = require ( "async-redis" );
const uuid4 = require ( "uuid4" );



const I_REDIS_PORT = 6379;
const I_PROCESSING_MS_TIME_INTERVAL = 1000;
const I_PROCESSING_MS_TIME_OFFSET = 100;

const I_GENERATOR_AWAY_MAX_TIME = 8000;
const I_MAX_DATA_BASE_LOCK_TIME = 1000;

const I_MAX_GENERATION_VALUE = 10;
const I_MAX_RIGHT_VALUE = 8;

const EMode = {
	Generator : 0,
	Processor : 1
};

/*
  Generator previous heartbeat time.
 GeneratorProcessId.
 Message.
 Message time.
 Processed messages counter.
 Errors list.
 Database lock time.
 Database lock process id.
*/



class TWorkerProcess {
	constructor () {
		this.m_sId = uuid4 ();

		this.m_pRedisClient = redis.createClient ( I_REDIS_PORT );
		this.m_pRedisClient.on ( "error", function ( err ) {
			console.error ( "TWorkerProcess.constructor () : m_pRedisClient : On Error : ", err, " ." );
		} );

		this.m_pAsyncRedisClient = moduleAsyncRedis.createClient ();
		// this.m_pAsyncRedisClient.on ( "error", function ( err ) {
		// 	console.error ( "TWorkerProcess.constructor () : m_pAsyncRedisClient : On Error : ", err, " ." );
		// } );

		// this.m_eMode = EMode.Processor;
		this.BecomeProcessor ();

		this.m_pProcessingTimer = null;

		// this.m_bIsProcessingOfPreviousIterationComplete = true;
	}

	ParseGetParameters ( _pRequest, _pCtx, _aQuery ) {

	}

	Run () {
		setTimeout ( () => {
			this.m_pProcessingTimer = setInterval ( this.Process.bind ( this ), I_PROCESSING_MS_TIME_INTERVAL );
			}, Math.round ( Math.random () * I_PROCESSING_MS_TIME_OFFSET ) );
	}

	async Process () {
		// if ( ! this.m_bIsProcessingOfPreviousIterationComplete )
		// 	return;



		// this.m_bIsProcessingOfPreviousIterationComplete = false;

		if ( this.m_eMode === EMode.Generator ) {
			console.log ( "Process ", this.m_sId, " is a generator." );
			// console.log ( await this.CheckWhetherThereIsAlreadyNewGenerator () );
			if ( ! await this.CheckWhetherThereIsAlreadyNewGenerator () ) {
				await this.LockDataBase ();
				await this.SendGeneratorHeartBeat ();
				await this.SendGeneratedMessage ();
				await this.UnLockDataBase ();

				// setTimeout ( this.BecomeProcessor.bind ( this ), 4000 );
			} else {
				this.BecomeProcessor ();
			} //-else

		} else {
			console.log ( "Process ", this.m_sId, " is a processor." );

			// this.m_pRedisClient.hget ( "Generator", "iPreviousHeartBeatTime", function ( err, _variantData ) {
			// 		if ( err ) {
			// 			console.error ( err );
			// 			return;
			// 		}

			// 		// console.log ( _variantData );
					
			// 		  If ( there is no Generator up to this time or previous heart beat from Generator was long ago ) and
			// 		 Data Base is not locked or it is locked for too long - in this case we relock Data Base with our id
			// 		 and current UTC time.
					
			// 		if ( ( ! _variantData || ! this.CheckPreviousGeneratorHeartBeatTime ( _variantData ) ) &&
			// 		     ( ! this.IsDataBaseLocked () || ! this.CheckDataBaseLockTime () ) ) {
			// 			this.LockDataBase ( () => {
			// 					this.BecomeGenerator ( () => {
			// 							this.UnLockDataBase ( () => {
			// 									this.m_bIsProcessingOfPreviousIterationComplete = true;
			// 								} );
			// 						} );
			// 				} );
			// 		} //-if

			// 	} );

			//---Checking whether Generator is active and becoming it, if needed.-----------
			let iPreviousHeartBeatTime = await this.m_pAsyncRedisClient.hget ( "Generator", "iHeartBeatTime" );
			// console.log ( this.m_pAsyncRedisClient );
			// console.log ( "TWorkerProcess.Process ()"
			// 	, " : ", Date.now ()
			// 	, ", ", ( Date.now () - iPreviousHeartBeatTime )
			// 	, ", iPreviousHeartBeatTime : ", iPreviousHeartBeatTime
			// 	, ", CheckPreviousGeneratorHeartBeatTime () : ", await this.CheckPreviousGeneratorHeartBeatTime ( iPreviousHeartBeatTime )
			// 	, "\n, IsDataBaseLocked () : ", await this.IsDataBaseLocked ()
			// 	, ", CheckDataBaseLockTime ()", await this.CheckDataBaseLockTime ()
			// 	, "\n- ", ( ! iPreviousHeartBeatTime || ! await this.CheckPreviousGeneratorHeartBeatTime ( iPreviousHeartBeatTime ) )
			//     , " , ", ( ! await this.IsDataBaseLocked () || ! await this.CheckDataBaseLockTime () )
			// 	, " ." );

			if ( ( ! iPreviousHeartBeatTime || ! await this.CheckPreviousGeneratorHeartBeatTime ( iPreviousHeartBeatTime ) ) &&
			     ( ! await this.IsDataBaseLocked () || ! await this.CheckDataBaseLockTime () ) ) {
				await this.LockDataBase ();
				await this.BecomeGenerator ();
				await this.SendGeneratorHeartBeat ();
				await this.UnLockDataBase ();
			} //-if

			//---Processing message from generator.----------------------------------------
			this.ReadGeneratedMessage ();

			// this.m_bIsProcessingOfPreviousIterationComplete = true;
		} //-else
	}



	async CheckPreviousGeneratorHeartBeatTime ( _timeHeartBeat ) {
		// let dateCurrent = new Date ();

		return Date.now () - _timeHeartBeat < I_GENERATOR_AWAY_MAX_TIME;
	}

	async BecomeGenerator () {
		console.log ( "TWorkerProcess.BecomeGenerator () : Process", this.m_sId, " becomes a Generator." );
		await this.m_pAsyncRedisClient.hset ( "Generator", "sId", this.m_sId );
		this.m_eMode = EMode.Generator;
	}

	async CheckWhetherThereIsAlreadyNewGenerator () {
		let sGeneratorId = await this.m_pAsyncRedisClient.hget ( "Generator", "sId" );

		return sGeneratorId != this.m_sId;
	}

	/*
	  Here we overwrite Generator id and heart beat time by our id and current time.
	*/
	async SendGeneratorHeartBeat () {
		await this.m_pAsyncRedisClient.hset ( "Generator", "iHeartBeatTime", Date.now () );

	}

	async SendGeneratedMessage () {
		await this.m_pAsyncRedisClient.hset ( "Generator", "iMessage", Math.round ( Math.random () * 11 ) );
	}

	BecomeProcessor () {
		console.log ( "TWorkerProcess.BecomeProcessor () : Process", this.m_sId, " becomes a Processor." );
		this.m_eMode = EMode.Processor;
	}



	async ReadGeneratedMessage () {
		let iMessage = await this.m_pAsyncRedisClient.hget ( "Generator", "iMessage" );

		if ( iMessage && ( iMessage <= I_MAX_RIGHT_VALUE ) ) {
			await this.WriteProcessedMessagesCounter ( iMessage );
		} else {
			await this.SignalAboutReceivingMessageWithError ( iMessage )
		} //-else
	}

	async WriteProcessedMessagesCounter ( _iMessage ) {
		let iAmountOfProcessedMessages = await this.m_pAsyncRedisClient.hget ( "Processors_ProcessedMessages", this.m_sId );

		iAmountOfProcessedMessages = iAmountOfProcessedMessages > 0 ? Number ( iAmountOfProcessedMessages ) + 1 : 1;

		// console.log ( "TWorkerProcess.WriteProcessedMessagesCounter () : We ", this.m_sId, " received correct message from generator "
		// 	, _iMessage
		// 	,",\n and already processed ", iAmountOfProcessedMessages, " messages"
		// 	, "." );

		await this.LockDataBase ();
		await this.m_pAsyncRedisClient.hset ( "Processors_ProcessedMessages", this.m_sId, iAmountOfProcessedMessages );
		await this.UnLockDataBase ();
	}

	async SignalAboutReceivingMessageWithError ( _iMessage ) {
		let iAmountOfReceivedMessagesWithErrors = await this.m_pAsyncRedisClient.hget ( "Processors_ReceivedMessagesWithErrors", this.m_sId );

		iAmountOfReceivedMessagesWithErrors = iAmountOfReceivedMessagesWithErrors > 0 ? Number ( iAmountOfReceivedMessagesWithErrors ) + 1 : 1;

		// console.warn ( "TWorkerProcess.SignalAboutReceivingMessageWithError () : We ", this.m_sId, " received not correct message from generator "
		// 	, _iMessage
		// 	,",\n and already received ", iAmountOfReceivedMessagesWithErrors, " messages with errors"
		// 	, "." );

		await this.LockDataBase ();
		await this.m_pAsyncRedisClient.hset ( "Processors_ReceivedMessagesWithErrors", this.m_sId, iAmountOfReceivedMessagesWithErrors );
		await this.UnLockDataBase ();
	}



	async IsDataBaseLocked () {
		// console.log ( this.m_pAsyncRedisClient );

		return await this.m_pAsyncRedisClient.hget ( "DataBase", "iIsLocked" ) === "1";
	}

	async CheckDataBaseLockTime () {
		let iDataBasLockTime = await this.m_pAsyncRedisClient.hget ( "DataBase", "iLockTime" );
		return Date.now () - iDataBasLockTime < I_MAX_DATA_BASE_LOCK_TIME;
	}

	async LockDataBase () {
		await this.m_pAsyncRedisClient.hset ( "DataBase", "iIsLocked", 1 );
		await this.m_pAsyncRedisClient.hset ( "DataBase", "iLockTime", Date.now () );
	}

	async UnLockDataBase () {
		await this.m_pAsyncRedisClient.hset ( "DataBase", "iIsLocked", 0 );
	}

}



const g_pWorkerProcess = new TWorkerProcess ();

g_pWorkerProcess.Run ();