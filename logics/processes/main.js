const cluster = require ( 'cluster' );
const path = require ( 'path' );



const I_AMOUNT_OF_WORKER_PROCESSES = 10;

const I_MAIN_PROCESS_UPDATE_INTERVAL = 2000;



class TMainProcess {
	constructor () {
		this.m_bClosed = false;

		cluster.setupMaster({
			// exec: path.join(__dirname, 'pool-worker.js')
			exec : path.join ( __dirname, "./worker.js" )
		});

		cluster.on ( 'fork', this.OnWorkerFork.bind ( this ) );
		cluster.on ( 'exit', this.OnWorkerExit.bind ( this ) );

		this.m_pUpdateTimer = setInterval ( this.Update.bind ( this ), I_MAIN_PROCESS_UPDATE_INTERVAL );

		this.LaunchWorkerProcesses ();
	}

	ParseGetParameters ( _pRequest, _pCtx, _aQuery ) {

	}



	LaunchWorkerProcesses () {
		for ( let i = 0; i < I_AMOUNT_OF_WORKER_PROCESSES; i ++ ) {
			cluster.fork ();   // this._getWorkerConfig()
		} //-for
	}

	Close () {
		this.m_bClosed = true;
	}

	Update () {
		console.log ( "TMainProcess.Update () : Updating main process." );
	}



	OnWorkerReady ( worker ) {
		console.log ( 'TMainProcess.OnWorkerReady () : Worker process ' + worker.process.pid + ' is ready.' );

		// for (var id in this._webinars) {
		// 	if (this._webinars.hasOwnProperty(id)) {
		// 		// Отправим всем сообщение слушать вебинар
		// 		this.listenWebinar(id);
		// 	} //-if
		// } //-for
	}

	OnWorkerFork ( worker ) {
		console.warn ( 'TMainProcess.OnWorkerFork () : Worker process ' + worker.process.pid + " has started." );
	}

	OnWorkerExit ( worker, code ) {
		console.warn ( 'TMainProcess.OnWorkerExit () : Worker process ' + worker.process.pid + ' exited with code ' + code + " ." );

		if ( this.m_bClosed ) {
			return;
		}

		cluster.fork ();  // this._getWorkerConfig ()
	}

}



module.exports = TMainProcess;